import os

import numpy as np

import torch
from albumentations import (
    CLAHE,
    Blur,
    Compose,
    GridDistortion,
    HorizontalFlip,
    Normalize,
    OpticalDistortion,
    RandomBrightnessContrast,
    RandomResizedCrop,
    RandomRotate90,
    Resize,
    Rotate,
    ShiftScaleRotate,
    Transpose,
    VerticalFlip,
)
from albumentations.pytorch import ToTensorV2
from bengaliai.backbones import get_model
from bengaliai.callbacks import CleanupCallback, ComputeValLoss, MetricsCallback, PredictCallback
from bengaliai.dataset import BengaliaiDataset
from bengaliai.transforms import OneToThree
from torch.optim import Adam
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.utils.data import DataLoader

name = "BASELINE_noresize"
BATCH_SIZE = 30


augs = Compose([OneToThree(), ToTensorV2()])  # Resize(224, 224),

callbacks = (
    ComputeValLoss(),
    PredictCallback(),
    MetricsCallback(),
    CleanupCallback(train_step=["loss"], validation_end=["mean_val_loss", "val_score"]),
)

config = {
    "name": name,
    "callbacks": callbacks,
    "data": {
        "train_df_path": "/data/train.csv",
        "train_parquets": (
            "/data/train_image_data_0.parquet",
            "/data/train_image_data_1.parquet",
            "/data/train_image_data_2.parquet",
            "/data/train_image_data_3.parquet",
        ),
    },
    # "debug": {"df_num_samples": 1000},
    "cv": {"generator_kwargs": {"n_splits": 5, "random_state": 0}, "using_splits": 2},
    "backbone": {"base_class": get_model, "kwargs": {"out": 186}},
    "opter_constructor": {"base_class": Adam, "kwargs": {"lr": 1e-4}},
    "loss": {"base_class": torch.nn.BCELoss},
    "stages": [
        # 0 stage
        {
            "train": {
                "dataset_constructor": {"base_class": BengaliaiDataset, "kwargs": {"labels": True, "transform": augs}},
                "dataloader_constructor": {
                    "base_class": DataLoader,
                    "kwargs": {"num_workers": 32, "pin_memory": True, "batch_size": BATCH_SIZE, "shuffle": True},
                },
            },
            "val": {
                "dataset_constructor": {"base_class": BengaliaiDataset, "kwargs": {"labels": True, "transform": augs}},
                "dataloader_constructor": {
                    "base_class": DataLoader,
                    "kwargs": {"num_workers": 32, "pin_memory": True, "batch_size": BATCH_SIZE, "shuffle": False},
                },
            },
            "test": {
                "dataset_constructor": {"base_class": BengaliaiDataset, "kwargs": {"labels": True, "transform": augs}},
                "dataloader_constructor": {
                    "base_class": DataLoader,
                    "kwargs": {"num_workers": 32, "pin_memory": True, "batch_size": BATCH_SIZE, "shuffle": False},
                },
            },
        }
    ],
    "lightning_callbacks": {
        "checkpoint_path": "/src/workspace/models",
        "ModelCheckpoint_kwargs": {
            "save_best_only": 1,
            "verbose": True,
            "monitor": "val_score",
            "mode": "max",
            "prefix": "",
        },
        "EarlyStopping_kwargs": {
            "monitor": "val_score",
            "min_delta": 0.00,
            "patience": 2,
            "verbose": True,
            "mode": "max",
        },
        "logger_base_dir": "/src/workspace/exp",
    },
    "trainer": {
        "max_nb_epochs": 100,
        "use_amp": False,
        "gpus": [0, 1],
        "distributed_backend": "ddp",
        # "train_percent_check": 1,
        # "log_gpu_memory": "all",
        # "fast_dev_run": True,
    },
}
