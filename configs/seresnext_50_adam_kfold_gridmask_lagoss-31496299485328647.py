import os

import numpy as np

import torch
from albumentations import (
    CLAHE,
    Blur,
    Compose,
    GridDistortion,
    HorizontalFlip,
    Normalize,
    OpticalDistortion,
    RandomBrightnessContrast,
    RandomResizedCrop,
    RandomRotate90,
    Resize,
    Rotate,
    ShiftScaleRotate,
    Transpose,
    VerticalFlip,
)
from albumentations.pytorch import ToTensorV2
from bengaliai.backbones import get_model, get_resnet_34, get_seresnext50
from bengaliai.callbacks import CleanupCallback, ComputeValLoss, MetricsCallback, PredictCallback
from bengaliai.dataset import BengaliaiDataset, BengaliaiDatasetMemmap
from bengaliai.data_utils import Cutmix, MixCollate

from bengaliai.transforms import OneToThree, GridMask, LafossResize
from bengaliai.legacy.over9000.over9000 import RangerLars
from torch.optim import Adam
from apex.optimizers import FusedNovoGrad, FusedAdam
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.utils.data import DataLoader

name = "seresnext_50_adam_kfold_gridmask_lagoss"
BATCH_SIZE = 60

augs = Compose([LafossResize(), OneToThree(), GridMask(num_grid=(3, 7), p=0.5), ToTensorV2()])  # Resize(224, 224),
augs_val = Compose([LafossResize(), OneToThree(), ToTensorV2()])

tr_collate = MixCollate(Cutmix(), values={2: 50, 1: 10})

callbacks = (
    ComputeValLoss(),
    PredictCallback(),
    MetricsCallback(),
    CleanupCallback(train_step=["loss"], validation_end=["mean_val_loss", "val_score"]),
)

model_config = {
    "callbacks": callbacks,
    "backbone": {"base_class": get_seresnext50, "kwargs": {"out": 186, "pretrained": None}},
    "use_sigmoid": False,
    # "load_weights_from": "/src/workspace/models/BASELINE_noresize_memmap2954064364467459743_fold_0/_ckpt_epoch_16.ckpt",
    "opter_constructor": {"base_class": FusedAdam, "kwargs": {"lr": 2e-4}},
    "loss": {"base_class": torch.nn.BCEWithLogitsLoss},
    "stages": [
        # 0 stage
        {
            "train": {
                "dataset_constructor": {
                    "base_class": BengaliaiDataset,
                    "kwargs": {
                        "labels": True,
                        "transform": augs,
                        "mem_name": "/data_fast/data/bengaliai/train_dump_memmap.npy",
                    },
                },
                "dataloader_constructor": {
                    "base_class": DataLoader,
                    "kwargs": {
                        "num_workers": 32,
                        "pin_memory": False,
                        "batch_size": BATCH_SIZE,
                        "shuffle": True,
                        # "collate_fn": tr_collate,
                    },
                },
            },
            "val": {
                "dataset_constructor": {
                    "base_class": BengaliaiDataset,
                    "kwargs": {
                        "labels": True,
                        "transform": augs_val,
                        "mem_name": "/data_fast/data/bengaliai/val_dump_memmap.npy",
                    },
                },
                "dataloader_constructor": {
                    "base_class": DataLoader,
                    "kwargs": {
                        "num_workers": 32,
                        "pin_memory": False,
                        "batch_size": int(2 * BATCH_SIZE),
                        "shuffle": False,
                    },
                },
            },
        }
    ],
}

data_config = {
    # "debug": {"df_num_samples": 1000},
    "data": {
        "train_df_path": "/data/train.csv",
        "train_parquets": (
            "/data/train_image_data_0.parquet",
            "/data/train_image_data_1.parquet",
            "/data/train_image_data_2.parquet",
            "/data/train_image_data_3.parquet",
        ),
        "train_parquets_mem": "/data_fast/data/bengaliai/train_dump_memmap.npy",
        "val_parquets_mem": "/data_fast/data/bengaliai/val_dump_memmap.npy",
    }
}

training_config = {
    "name": name,
    "cv": {"generator_kwargs": {"n_splits": 5, "random_state": 0}, "using_splits": 1},
    "lightning_callbacks": {
        "checkpoint_path": "/src/workspace/models",
        "ModelCheckpoint_kwargs": {"verbose": True, "monitor": "val_score", "mode": "max", "prefix": ""},
        "EarlyStopping_kwargs": {
            "monitor": "val_score",
            "min_delta": 0.00,
            "patience": 10,
            "verbose": True,
            "mode": "max",
        },
        "logger_base_dir": "/src/workspace/exp",
        "TestTubeLogger_kwargs": {"version": 0},
    },
    "trainer": {
        "max_nb_epochs": 400,
        # "use_amp": True,
        "gpus": [0, 1, 2],  # 27
        "distributed_backend": "dp",
        # "opt_level": "O2"
        # "train_percent_check": 1,
        # "log_gpu_memory": "all",
        # "fast_dev_run": True,
    },
}
