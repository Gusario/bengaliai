import os

import numpy as np

import torch
from albumentations import (
    CLAHE,
    Blur,
    Compose,
    GridDistortion,
    HorizontalFlip,
    Normalize,
    OpticalDistortion,
    RandomBrightnessContrast,
    RandomResizedCrop,
    RandomRotate90,
    Resize,
    Rotate,
    ShiftScaleRotate,
    Transpose,
    VerticalFlip,
    ElasticTransform,
    RandomContrast,
    RandomGamma,
    RandomBrightness,
    OneOf,
)
from albumentations.pytorch import ToTensorV2
from apex.optimizers import FusedAdam
from bengaliai.backbones import EfficientnetB3ns, SeResnext50Backbone
from bengaliai.callbacks import CleanupCallback, ComputeValLoss, MetricsCallback, PredictCallback
from bengaliai.custom_validation import CustomValidation
from bengaliai.dataset import BengaliaiDataset, BengaliaiDatasetMemmap
from bengaliai.mix import Cutmix, MixCollate, Mixup, PassMix
from bengaliai.model import BaseModel, FreezeSheduledModel
from bengaliai.transforms import GridMask, OneToThree

# from bengaliai.legacy.over9000.over9000 import
from torch.optim import SGD
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.utils.data import DataLoader
from warmup_scheduler import GradualWarmupScheduler

name = "efficient_b3a_cutmix_second_midaugs_cutmix"
BATCH_SIZE = 40

augs = Compose(
    [
        OneOf([RandomContrast(), RandomGamma(), RandomBrightness()], p=0.3),
        OneOf([GridMask(num_grid=(3, 7)), GridMask(num_grid=(3, 7), mode=1)], p=0.5),
        ShiftScaleRotate(),
        Resize(300, 300),
        OneToThree(),
        ToTensorV2(),
    ]
)
augs_val = Compose([Resize(300, 300), OneToThree(), ToTensorV2()])

tr_collate = MixCollate(mix_classes=[Cutmix(), Mixup(2)], values=[30, 10])

callbacks = (
    ComputeValLoss(),
    PredictCallback(),
    MetricsCallback(),
    CleanupCallback(train_step=["loss"], validation_end=["mean_val_loss", "val_score", "progress_bar"]),
)

model_config = {
    "callbacks": callbacks,
    "base_model": BaseModel,
    "backbone": {
        "base_class": EfficientnetB3ns(),
        "kwargs": {
            "out": 186,
            "pretrained": True,
            # "avg_pool_size": 7,
            "weights_path": "/src/workspace/models/efficient_b3a_cutmix5583762746496336843_fold_1/_ckpt_epoch_41.ckpt",
        },
    },
    "use_sigmoid": False,
    "opter_constructor": {"base_class": FusedAdam, "kwargs": {"lr": 3e-4}},
    "scheduler_constructor": {"base_class": GradualWarmupScheduler, "kwargs": {"multiplier": 8, "total_epoch": 2}},
    # "warmup_scheduler_constructor": {"base_class": GradualWarmupScheduler, "kwargs": {"multiplier":8, "total_epoch":10}},
    "loss": {"base_class": torch.nn.BCEWithLogitsLoss},
    "stages": [
        # 0 stage
        {
            "train": {
                "dataset_constructor": {
                    "base_class": BengaliaiDataset,
                    "kwargs": {
                        "labels": True,
                        "transform": augs,
                        "mem_name": "/data_fast/data/bengaliai/train_dump_memmap.npy",
                    },
                },
                "dataloader_constructor": {
                    "base_class": DataLoader,
                    "kwargs": {
                        "num_workers": 0,
                        "pin_memory": False,
                        "batch_size": BATCH_SIZE,
                        "shuffle": True,
                        "collate_fn": tr_collate,
                        "drop_last": True,
                    },
                },
            },
            "val": {
                "dataset_constructor": {
                    "base_class": BengaliaiDataset,
                    "kwargs": {
                        "labels": True,
                        "transform": augs_val,
                        "mem_name": "/data_fast/data/bengaliai/val_dump_memmap.npy",
                    },
                },
                "dataloader_constructor": {
                    "base_class": DataLoader,
                    "kwargs": {"num_workers": 0, "pin_memory": False, "batch_size": int(BATCH_SIZE), "shuffle": False},
                },
            },
        }
    ],
}

data_config = {
    # "debug": {"df_num_samples": 1000},
    "data": {
        "train_df_path": "/data/train.csv",
        "train_parquets": (
            "/data/train_image_data_0.parquet",
            "/data/train_image_data_1.parquet",
            "/data/train_image_data_2.parquet",
            "/data/train_image_data_3.parquet",
        ),
        "train_parquets_mem": "/data_fast/data/bengaliai/train_dump_memmap.npy",
        "val_parquets_mem": "/data_fast/data/bengaliai/val_dump_memmap.npy",
    }
}

training_config = {
    "name": name,
    "cv": {"base_class": CustomValidation, "kwargs": {"n_splits": 5, "n_out": 40}, "using_splits": [1]},
    "lightning_callbacks": {
        "checkpoint_path": "/src/workspace/models",
        "ModelCheckpoint_kwargs": {"verbose": True, "monitor": "val_score", "mode": "max", "prefix": ""},
        "EarlyStopping_kwargs": {
            "monitor": "val_score",
            "min_delta": 0.00,
            "patience": 5,
            "verbose": True,
            "mode": "max",
        },
        "logger_base_dir": "/src/workspace/exp",
        "TestTubeLogger_kwargs": {"version": 0},
    },
    "trainer": {
        "max_nb_epochs": 400,
        # "use_amp": True,
        "gpus": [0, 1, 2],  # 27
        "distributed_backend": "ddp",
        # "amp_level": "O2"
        # "train_percent_check": 1,
        # "log_gpu_memory": "all",
        # "fast_dev_run": True,
    },
}
