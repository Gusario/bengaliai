import os

import numpy as np

import torch
from albumentations import (
    Compose,
    RandomResizedCrop,
    RandomRotate90,
    Resize,
    Rotate,
    ShiftScaleRotate,
    Transpose,
    VerticalFlip,
    ElasticTransform,
    RandomContrast,
    RandomGamma,
    RandomBrightness,
    OneOf,
)
from albumentations.pytorch import ToTensorV2
from apex.optimizers import FusedAdam
from bengaliai.backbones import EfficientnetB3ns, SeResnext50Backbone
from bengaliai.callbacks import CleanupCallback, ComputeValLoss, MetricsCallback, PredictCallback
from bengaliai.custom_validation import CustomValidation
from bengaliai.dataset import BengaliaiDataset, BengaliaiDatasetMemmap
from bengaliai.mix import Cutmix, MixCollate, Mixup, PassMix, CutmixDoubleLoss, MixupDoubleLoss
from bengaliai.model import BaseModel, FreezeSheduledModel, WeightedSoftmax
from bengaliai.positional_encoding import TwoMatrixPositionalEncoding
from bengaliai.transforms import GridMask, OneToThree, StackMatrices, LabelSmoothing

# from bengaliai.legacy.over9000.over9000 import
from torch.optim import SGD
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.utils.data import DataLoader
from warmup_scheduler import GradualWarmupScheduler

name = "se_resnext_50_midaug_cutmixdoubleloss_cos_sgd_labelsmooth_second_adam"
BATCH_SIZE = 340

augs = Compose(
    [
        OneOf([RandomContrast(p=1), RandomGamma(p=1), RandomBrightness(p=1)], p=0.3),
        GridMask(num_grid=(3, 7), p=0.5),
        ShiftScaleRotate(),
        Resize(224, 224),
        OneToThree(),
        ToTensorV2(),
    ]
)
augs_val = Compose([Resize(224, 224), OneToThree(), ToTensorV2()])

transform_y = LabelSmoothing(eps=1e-4, K=186, len_positive_labels=3)

tr_collate = MixCollate(mix_classes=[CutmixDoubleLoss()], values=[340], double_loss=True, beta=1)

callbacks = (
    ComputeValLoss(),
    PredictCallback(),
    MetricsCallback(),
    CleanupCallback(train_step=["loss"], validation_end=["val_loss", "val_score", "progress_bar"]),
)

model_config = {
    "callbacks": callbacks,
    "base_model": BaseModel,
    "backbone": {
        "base_class": SeResnext50Backbone(),
        "kwargs": {
            "out": 186,
            "pretrained": "imagenet",
            "avg_pool_size": 7,
            # "drop_rate": 0.5,
            "weights_path": "/src/workspace/models/_ckpt_epoch_197.ckpt",
        },
    },
    "use_sigmoid": False,
    "opter_constructor": {"base_class": FusedAdam, "kwargs": {"lr": 0.001}},
    "scheduler_constructor": {
        "base_class": torch.optim.lr_scheduler.ReduceLROnPlateau,
        "kwargs": {"patience": 10, "threshold": 0.000000001, "verbose": True, "factor": 0.3},
    },
    # "scheduler_constructor": {"base_class": torch.optim.lr_scheduler.CosineAnnealingLR, "kwargs": {"T_max": 200}},
    # "gradual_warmup_lr": {"base_class": GradualWarmupScheduler, "kwargs": {"multiplier": 10, "total_epoch": 5}},
    "loss": {"base_class": torch.nn.BCEWithLogitsLoss},
    "stages": [
        # 0 stage
        {
            "train": {
                "dataset_constructor": {
                    "base_class": BengaliaiDataset,
                    "kwargs": {
                        "labels": True,
                        "transform": augs,
                        "transform_y": transform_y,
                        "mem_name": "/data_fast/data/bengaliai/train_dump_memmap.npy",
                    },
                },
                "dataloader_constructor": {
                    "base_class": DataLoader,
                    "kwargs": {
                        "num_workers": 16,
                        "pin_memory": False,
                        "batch_size": BATCH_SIZE,
                        "shuffle": True,
                        "collate_fn": tr_collate,
                        "drop_last": True,
                    },
                },
            },
            "val": {
                "dataset_constructor": {
                    "base_class": BengaliaiDataset,
                    "kwargs": {
                        "labels": True,
                        "transform": augs_val,
                        "mem_name": "/data_fast/data/bengaliai/val_dump_memmap.npy",
                    },
                },
                "dataloader_constructor": {
                    "base_class": DataLoader,
                    "kwargs": {
                        "num_workers": 16,
                        "pin_memory": False,
                        "batch_size": int(2 * BATCH_SIZE),
                        "shuffle": False,
                    },
                },
            },
        }
    ],
}

data_config = {
    # "debug": {"df_num_samples": 1000},
    "data": {
        # "train_df_path": "/data/extended_train6.csv",
        "train_df_path": "/data/train.csv",
        "train_parquets": (
            "/data/train_image_data_0.parquet",
            "/data/train_image_data_1.parquet",
            "/data/train_image_data_2.parquet",
            "/data/train_image_data_3.parquet",
            # "/data/matr1.parquet",
            # "/data/matr2.parquet",
            # "/data/matr3.parquet",
            # "/data/matr4.parquet",
            # "/data/matr5.parquet",
            # "/data/matr6.parquet"
        ),
        "train_parquets_mem": "/data_fast/data/bengaliai/train_dump_memmap.npy",
        "val_parquets_mem": "/data_fast/data/bengaliai/val_dump_memmap.npy",
    },
    "pseudolabels": False,
}

training_config = {
    "name": name,
    "pseudo": False,
    "cv": {"base_class": CustomValidation, "kwargs": {"n_splits": 5, "n_out": 30}, "using_splits": [2]},
    "lightning_callbacks": {
        "dirpath": "/src/workspace/models",
        "ModelCheckpoint_kwargs": {"verbose": 1, "monitor": "val_score", "mode": "max"},  # "save_best_only": True,
        "EarlyStopping_kwargs": {
            "monitor": "val_score",
            "min_delta": 0.00,
            "patience": 200,
            "verbose": 1,
            "mode": "max",
        },
        "logger_base_dir": "/src/workspace/exp",
        "TestTubeLogger_kwargs": {"version": 0},
        # "Wandb_kwargs": {"project": "bengaliai"}
    },
    "trainer": {
        "max_nb_epochs": 1000,
        # "use_amp": True,
        "gpus": [0, 1, 2],  # 27
        "distributed_backend": "dp",
        "accumulate_grad_batches": 5,
        # "amp_level": "O2"
        # "train_percent_check": 1,
        # "log_gpu_memory": "all",
        # "fast_dev_run": True,
        # "resume_from_checkpoint": "/src/workspace/models/_ckpt_epoch_117.ckpt",
    },
}
