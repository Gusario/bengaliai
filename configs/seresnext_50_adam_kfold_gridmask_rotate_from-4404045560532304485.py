import os

import numpy as np

import torch
from albumentations import (
    CLAHE,
    Blur,
    Compose,
    GridDistortion,
    HorizontalFlip,
    Normalize,
    OpticalDistortion,
    RandomBrightnessContrast,
    RandomResizedCrop,
    RandomRotate90,
    Resize,
    Rotate,
    ShiftScaleRotate,
    Transpose,
    VerticalFlip,
)
from albumentations.pytorch import ToTensorV2
from bengaliai.backbones import get_model, get_resnet_34, SeResnext50Backbone
from bengaliai.callbacks import CleanupCallback, ComputeValLoss, MetricsCallback, PredictCallback
from bengaliai.dataset import BengaliaiDataset, BengaliaiDatasetMemmap
from bengaliai.mix import Cutmix, MixCollate, Mixup

from bengaliai.transforms import OneToThree, GridMask, LafossResize
from bengaliai.legacy.over9000.over9000 import RangerLars
from torch.optim import Adam, SGD
from apex.optimizers import FusedNovoGrad, FusedAdam
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.utils.data import DataLoader

name = "seresnext_50_adam_kfold_gridmask_rotate_from"
BATCH_SIZE = 70

augs = Compose(
    [Resize(224, 224), OneToThree(), Rotate(limit=25, p=0.75), GridMask(num_grid=(3, 7), p=0.5), ToTensorV2()]
)  # Resize(224, 224),
augs_val = Compose([Resize(224, 224), OneToThree(), ToTensorV2()])

# tr_collate = MixCollate(mix_classes=[Cutmix(), Mixup(2), Mixup(3)], values=[80, 80, 90])

callbacks = (
    ComputeValLoss(),
    PredictCallback(),
    MetricsCallback(),
    CleanupCallback(train_step=["loss"], validation_end=["mean_val_loss", "val_score"]),
)

model_config = {
    "callbacks": callbacks,
    "backbone": {
        "base_class": SeResnext50Backbone(),
        "kwargs": {
            "out": 186,
            "pretrained": "imagenet",
            "avg_pool_size": 7,
            "weights_path": "/src/workspace/models/seresnext_50_adam_kfold_gridmask_newcutmix_rotate_mixup_from-2746860314361758193_fold_0/_ckpt_epoch_39.ckpt",
        },
    },
    "use_sigmoid": False,
    "opter_constructor": {"base_class": FusedAdam, "kwargs": {"lr": 3e-4}},
    "loss": {"base_class": torch.nn.BCEWithLogitsLoss},
    "stages": [
        # 0 stage
        {
            "train": {
                "dataset_constructor": {
                    "base_class": BengaliaiDataset,
                    "kwargs": {
                        "labels": True,
                        "transform": augs,
                        "mem_name": "/data_fast/data/bengaliai/train_dump_memmap.npy",
                    },
                },
                "dataloader_constructor": {
                    "base_class": DataLoader,
                    "kwargs": {
                        "num_workers": 0,
                        "pin_memory": False,
                        "batch_size": BATCH_SIZE,
                        "shuffle": True,
                        # "collate_fn": tr_collate,
                        "drop_last": True,
                    },
                },
            },
            "val": {
                "dataset_constructor": {
                    "base_class": BengaliaiDataset,
                    "kwargs": {
                        "labels": True,
                        "transform": augs_val,
                        "mem_name": "/data_fast/data/bengaliai/val_dump_memmap.npy",
                    },
                },
                "dataloader_constructor": {
                    "base_class": DataLoader,
                    "kwargs": {
                        # "num_workers": 32,
                        "pin_memory": False,
                        "batch_size": int(BATCH_SIZE),
                        "shuffle": False,
                    },
                },
            },
        }
    ],
}

data_config = {
    # "debug": {"df_num_samples": 1000},
    "data": {
        "train_df_path": "/data/train.csv",
        "train_parquets": (
            "/data/train_image_data_0.parquet",
            "/data/train_image_data_1.parquet",
            "/data/train_image_data_2.parquet",
            "/data/train_image_data_3.parquet",
        ),
        "train_parquets_mem": "/data_fast/data/bengaliai/train_dump_memmap.npy",
        "val_parquets_mem": "/data_fast/data/bengaliai/val_dump_memmap.npy",
    }
}

training_config = {
    "name": name,
    "cv": {"generator_kwargs": {"n_splits": 5, "random_state": 0}, "using_splits": 1},
    "lightning_callbacks": {
        "checkpoint_path": "/src/workspace/models",
        "ModelCheckpoint_kwargs": {"verbose": True, "monitor": "val_score", "mode": "max", "prefix": ""},
        "EarlyStopping_kwargs": {
            "monitor": "val_score",
            "min_delta": 0.00,
            "patience": 4,
            "verbose": True,
            "mode": "max",
        },
        "logger_base_dir": "/src/workspace/exp",
        "TestTubeLogger_kwargs": {"version": 0},
    },
    "trainer": {
        "max_nb_epochs": 400,
        # "use_amp": True,
        "gpus": [0, 1, 2],  # 27
        "distributed_backend": "ddp",
        # "amp_level": "O2"
        # "train_percent_check": 1,
        # "log_gpu_memory": "all",
        # "fast_dev_run": True,
    },
}
