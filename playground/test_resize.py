import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import cv2


class LafossResize:
    def __init__(self):
        self.WIDTH = 236
        self.HEIGHT = 137
        self.SIZE = 128
        self.last_img = None

    def bbox(self, img):
        rows = np.any(img, axis=1)
        cols = np.any(img, axis=0)
        rmin, rmax = np.where(rows)[0][[0, -1]]
        cmin, cmax = np.where(cols)[0][[0, -1]]
        return rmin, rmax, cmin, cmax

    def crop_resize(self, img0, size=128, pad=16):
        # crop a box around pixels large than the threshold
        # some images contain line at the sides

        ymin, ymax, xmin, xmax = self.bbox(img0[5:-5, 5:-5] > 80)

        # cropping may cut too much, so we need to add it back
        xmin = xmin - 13 if (xmin > 13) else 0
        ymin = ymin - 10 if (ymin > 10) else 0
        xmax = xmax + 13 if (xmax < WIDTH - 13) else WIDTH
        ymax = ymax + 10 if (ymax < HEIGHT - 10) else HEIGHT
        img = img0[ymin:ymax, xmin:xmax]
        # remove lo intensity pixels as noise
        img[img < 28] = 0
        lx, ly = xmax - xmin, ymax - ymin
        l = max(lx, ly) + pad
        # make sure that the aspect ratio is kept in rescaling
        img = np.pad(img, [((l - ly) // 2,), ((l - lx) // 2,)], mode="constant")
        self.l
        return cv2.resize(img, (size, size))


HEIGHT = 137
WIDTH = 236
SIZE = 128
parquet_path = "/Users/david/Downloads/train_image_data_0.parquet"  # change

df = pd.read_parquet(parquet_path)
n_imgs = 8
fig, axs = plt.subplots(n_imgs, 2, figsize=(10, 5 * n_imgs))

lafoss = LafossResize()
for idx in range(n_imgs):
    # somehow the original input is inverted
    img0 = 255 - df.iloc[idx, 1:].values.reshape(HEIGHT, WIDTH).astype(np.uint8)
    # normalize each image by its max val
    img = (img0 * (255.0 / img0.max())).astype(np.uint8)
    img = lafoss.crop_resize(img)

    axs[idx, 0].imshow(img0)
    axs[idx, 0].set_title("Original image")
    axs[idx, 0].axis("off")
    axs[idx, 1].imshow(img)
    axs[idx, 1].set_title("Crop & resize")
    axs[idx, 1].axis("off")
plt.show()
