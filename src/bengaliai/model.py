from typing import Tuple

from .util import instance_anything
from .backbones import FreezeRoutine

import numpy as np
import pytorch_lightning as pl
import torch
import torch.nn.functional as F
from torch.optim import Adam
from torch.utils.data.distributed import DistributedSampler


class BaseModel(pl.LightningModule):
    """Base model interface, use callback for injecting new fuctionality"""

    def __init__(self, config=None):
        super(BaseModel, self).__init__()

        self._config = config
        self.__setup_defaults()
        self._callbacks = self._config["callbacks"]
        self.__sanity_check()
        self.__setup_loss()

    def __sanity_check(self):
        pass

    def __setup_defaults(self) -> None:
        """Setup non-staging vars"""
        if "backbone" in self._config:
            current_config = self._config["backbone"]
            self._model = instance_anything(current_config)

        self._use_sigmoid = self._config["use_sigmoid"]

        self.__predict_matrix = np.NaN
        self.__truth_matrix = np.NaN
        self._sigmoid = torch.nn.Sigmoid()
        self._softmax = torch.nn.Softmax()

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """Default forward pass"""
        preds = self._model(x)
        if self._use_sigmoid:
            preds = self._sigmoid(preds)
        return preds

    def _get_current_stage(self) -> int:
        return 0  # TODO

    def _eval_loss(self, y_hat: torch.Tensor, y: torch.Tensor) -> torch.Tensor:
        loss = self._loss(y_hat, y)
        return loss

    def _eval_mix_loss(self, y_hat: torch.Tensor, y: Tuple[torch.Tensor], lambda_: float) -> torch.Tensor:
        y_1, y_2 = y
        loss = self._loss(y_hat, y_1) * lambda_ + self._loss(y_hat, y_2) * (1 - lambda_)
        return loss

    def _get_batch(self, batch: dict) -> Tuple[torch.Tensor, torch.Tensor]:
        """return image and label"""
        return batch["image"], batch["labels"]

    def configure_optimizers(self):
        opter_config = self._config["opter_constructor"]
        opter = opter_config["base_class"](self.parameters(), **opter_config["kwargs"])

        if "scheduler_constructor" in self._config:
            scheduler_config = self._config["scheduler_constructor"]
            scheduler = scheduler_config["base_class"](opter, **scheduler_config["kwargs"])
            if "gradual_warmup_lr" in self._config:
                gradual_config = self._config["gradual_warmup_lr"]
                scheduler = gradual_config["base_class"](opter, **gradual_config["kwargs"], after_scheduler=scheduler)
            return [opter], [scheduler]

        return opter

    def _get_dataloader(self, kind: str) -> torch.utils.data.DataLoader:
        """Construct dataloader, in future it must works with staging"""
        stage = self._get_current_stage()
        current_config = self._config["stages"][stage][kind]

        if "dataloader" not in current_config:
            # TODO: change on global registry or make dataset lightweight or delete old stages
            dataset = instance_anything(current_config["dataset_constructor"])
            current_config["dataloader_constructor"]["kwargs"]["dataset"] = dataset
            sampler = None
            if self.use_ddp:
                sampler = DistributedSampler(
                    dataset, shuffle=current_config["dataloader_constructor"]["kwargs"]["shuffle"]
                )
                del current_config["dataloader_constructor"]["kwargs"]["shuffle"]

            current_config["dataloader_constructor"]["kwargs"]["sampler"] = sampler

            dataloader = instance_anything(current_config["dataloader_constructor"])
            current_config["dataloader"] = dataloader
        return current_config["dataloader"]

    def set_predict_matrix(self, matrix) -> None:
        self.__predict_matrix = matrix.copy()

    def get_predict_matrix(self) -> np.array:
        return self.__predict_matrix

    def set_truth_matrix(self, matrix) -> None:
        self.__truth_matrix = matrix.copy()

    def get_truth_matrix(self) -> np.array:
        return self.__truth_matrix

    def freeze_conv(self):
        for param in self.parameters()[:-1]:
            param.requires_grad = False

    def __setup_loss(self) -> None:
        if "loss" in self._config:  # TODO mv to stages
            current_config = self._config["loss"]
            self._loss = instance_anything(current_config)

    def training_step(self, batch, batch_nb) -> dict:
        local_vars = {}
        local_vars["x"], local_vars["y"] = self._get_batch(batch)
        local_vars["y_hat"] = self.forward(local_vars["x"])

        if "lambda_" in batch:
            lambda_ = batch["lambda_"]
            local_vars["loss"] = self._eval_mix_loss(local_vars["y_hat"], local_vars["y"], lambda_)
        else:
            local_vars["loss"] = self._eval_loss(local_vars["y_hat"], local_vars["y"])

        for callback in self._callbacks:
            local_vars = callback.on_training_step(self, local_vars)
        return local_vars

    def validation_step(self, batch, batch_nb) -> dict:
        local_vars = {}
        local_vars["x"], local_vars["y"] = self._get_batch(batch)
        local_vars["y_hat"] = self.forward(local_vars["x"])

        for callback in self._callbacks:
            local_vars = callback.on_validation_step(self, local_vars)

        return local_vars

    def validation_end(self, outputs) -> dict:
        local_vars = {}
        for callback in self._callbacks:
            outputs, local_vars = callback.on_validation_end(self, outputs, local_vars)
        return local_vars

    @pl.data_loader
    def train_dataloader(self):
        dataloader = self._get_dataloader(kind="train")
        return dataloader

    @pl.data_loader
    def val_dataloader(self):
        dataloader = self._get_dataloader(kind="val")
        return dataloader


class FreezeSheduledModel(BaseModel):
    def on_epoch_start(self):
        fr = FreezeRoutine()
        epoch = self.trainer.current_epoch

        # if epoch in range(1):
        #     fr.freeze_all_exept_first(self._model)

        if epoch in range(2):
            fr.freeze_all_exept_fc(self._model)

        if epoch in range(2, 10):
            self.unfreeze()


class WeightedSoftmax(BaseModel):
    def _eval_loss(self, y_hat: torch.Tensor, y: torch.Tensor) -> torch.Tensor:

        grapheme_root_prob = y_hat[:, :168]
        grapheme_root_true = y[:, :168]
        # _, grapheme_root_true_idx = grapheme_root_true.max(dim=1)
        # grapheme_root_true = grapheme_root_true[grapheme_root_true_idx]
        grapheme_root_loss = self._loss(grapheme_root_prob, grapheme_root_true)

        vovel_deacritic_prob = y_hat[:, 168:179]
        vovel_deacritic_true = y[:, 168:179]
        # _, vovel_deacritic_true_idx = vovel_deacritic_true.max(dim=1)
        vovel_deacritic_loss = self._loss(vovel_deacritic_prob, vovel_deacritic_true)

        consonant_deacritic_prob = y_hat[:, 179:]
        consonant_deacritic_true = y[:, 179:]
        # _, vovel_deacritic_true_idx = consonant_deacritic_true.max(dim=1)
        consonant_deacritic_loss = self._loss(consonant_deacritic_prob, consonant_deacritic_true)

        loss = (consonant_deacritic_loss + vovel_deacritic_loss + grapheme_root_loss) / 3

        return loss
