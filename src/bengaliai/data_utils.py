import numpy as np
import pandas as pd


def read_parquets(parquets):
    parquet_DFs = []  # list of parquet's dataFrames
    for path in parquets:
        par_data = pd.read_parquet(path, engine="pyarrow")
        par_data = par_data.iloc[:, 1:].values
        parquet_DFs.append(par_data)
    return np.vstack(parquet_DFs)
