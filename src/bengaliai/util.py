import numpy as np
import pandas as pd
from .make_ohe import Ohe
import time


def make_submission(predicts):
    sub_dict = dict()
    for i, obj in enumerate(predicts):
        for count in range(3):
            name = "Test_" + str(i)
            if count == 0:
                local_name = name + "_consonant_diacritic"
                sub_dict[local_name] = obj[2]
            elif count == 1:
                local_name = name + "_grapheme_root"
                sub_dict[local_name] = obj[0]
            else:
                local_name = name + "_vowel_diacritic"
                sub_dict[local_name] = obj[1]

    submission = pd.DataFrame(list(sub_dict.items()), columns=["row_id", "target"])
    submission.to_csv(r"submission.csv", index=False, header=True)
    print(submission.head())


def convert_to_categorical(pred_matrix: np.ndarray, origin_dataset=True):
    # TODO Add flag to make pseudolabel dataset easier
    preds = np.array(pred_matrix)
    if origin_dataset is True:
        shape_y = preds.shape[0]
        graph_col = preds[:, :168].argmax(axis=1).reshape(shape_y, 1)
        vowel_col = preds[:, 168:179].argmax(axis=1).reshape(shape_y, 1)
        conson_col = preds[:, 179:186].argmax(axis=1).reshape(shape_y, 1)

        result_matrix = np.hstack((graph_col, vowel_col, conson_col))
        return result_matrix
    else:
        graph_col = preds[:, :168].argmax(axis=1)
        vowel_col = preds[:, 168:179].argmax(axis=1)
        conson_col = preds[:, 179:186].argmax(axis=1)
        return graph_col, vowel_col, conson_col


def rescore_pred_matrix(pred_matrix: np.ndarray, class_map: str, top=3, boost=2, cross_val_ans=None) -> np.ndarray:
    p_m = np.copy(pred_matrix)  #  rescored pred_matrix
    temp_matrix = np.zeros((pred_matrix.shape[0], top * 3))
    used = set()
    ohe = Ohe(class_map)
    if cross_val_ans is None:
        uniq_labels = np.load("../input/newdata2/all_uniq_triplets.npy")
    else:
        uniq_labels = ohe.get_uniq_labels(cross_val_ans)

    for idx, pred in enumerate(pred_matrix[:, :168]):
        ind = np.argpartition(pred, -top)[-top:]
        temp_matrix[idx, :top] = ind

    for idx, pred in enumerate(pred_matrix[:, 168:179]):
        ind = np.argpartition(pred, -top)[-top:]
        temp_matrix[idx, top : top * 2] = ind

    for idx, pred in enumerate(pred_matrix[:, 179:186]):
        ind = np.argpartition(pred, -top)[-top:]
        temp_matrix[idx, top * 2 : top * 3] = ind

    for idx, row in enumerate(temp_matrix):
        for i in range(top):
            for j in range(top, top * 2):
                for k in range(top * 2, top * 3):
                    if ohe.encode_label(np.array([int(row[i]), int(row[j]), int(row[k])])) in uniq_labels:
                        graph_id = int(row[i])
                        vowel_id = int(row[j]) + 168
                        conson_id = int(row[k]) + 179
                        if graph_id not in used:
                            p_m[idx][graph_id] *= boost
                            used.add(graph_id)
                        if vowel_id not in used:
                            p_m[idx][vowel_id] *= boost
                            used.add(vowel_id)
                        if conson_id not in used:
                            p_m[idx][conson_id] *= boost
                            used.add(conson_id)
        used.clear()

    return p_m


def instance_anything(values: dict) -> object:
    """Instance any obj, it takes base class and all args"""
    base_class = values["base_class"]
    if "kwargs" in values:
        kwargs = values["kwargs"]
    else:
        kwargs = {}
    return base_class(**kwargs)


def debug_parquet_multiplier(parquet):
    for i in range(16):
        parquet = np.vstack(parquet, parquet)
    return parquet
