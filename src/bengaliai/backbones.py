from abc import ABC, abstractmethod
from collections import OrderedDict
import os

from .legacy.pretrainedmodels import models as pretrainedmodels

try:
    import timm
except Exception:
    os.system("pip install ../input/newdata2/bengaliai/legacy/pytorch-image-models/")
    import timm

import timm
import torch
from torch import nn
import torchvision.models as models
from .legacy import pretrainedmodels
from collections import OrderedDict


class Backbone(ABC):
    """Looks like object abusing"""

    def __call__(self, **params):
        model = self.get_base_model(**params)
        if params.get("weights_path") is not None:
            model = self._load_weights(model, params["weights_path"])
        return model

    def _load_weights(self, model, path_to_checkpoint):
        checkpoint = torch.load(path_to_checkpoint, map_location=lambda storage, loc: storage)
        try:
            model.load_state_dict(checkpoint["state_dict"])
        except RuntimeError:
            state_dict = checkpoint["state_dict"]
            new_state_dict = OrderedDict()

            for k, v in state_dict.items():
                if "_model" in k:
                    k = k.replace("_model.", "")
                new_state_dict[k] = v
            model.load_state_dict(new_state_dict)
        return model

    @abstractmethod
    def get_base_model(self, **params):
        pass


class SeResnext50Backbone(Backbone):
    def get_base_model(self, **params):
        model = pretrainedmodels.se_resnext50_32x4d(
            num_classes=1000, pretrained=params["pretrained"], avg_pool_size=params["avg_pool_size"]
        )
        model.last_linear = torch.nn.Linear(2048, params["out"])
        return model


class SeResnext101Backbone(Backbone):
    def get_base_model(self, **params):
        model = pretrainedmodels.se_resnext101_32x4d(num_classes=1000, pretrained=params["pretrained"])
        model.last_linear = torch.nn.Linear(2048, params["out"])
        return model


class EfficientnetB3ns(Backbone):
    def get_base_model(self, **params):
        model = timm.create_model(
            "tf_efficientnet_b3_ns", pretrained=params["pretrained"], drop_rate=params["drop_rate"]
        )
        model.classifier = torch.nn.Linear(1536, params["out"])
        return model


class FreezeRoutine:
    """Works only with efficient net, need refactoring"""

    def freeze_all_exept_first(self, model):
        self.apply_all(model, freeze=True)

        for param in model.conv_stem.parameters():
            param.requires_grad = True

    def apply_all(self, model, freeze=True):
        for param in model.parameters():
            param.requires_grad = not freeze

    def freeze_all_exept_fc(self, model):
        self.apply_all(model, freeze=True)

        for param in model.classifier.parameters():
            param.requires_grad = True
