from cleanlab.pruning import get_noise_indices
import numpy as np


class NoiseIndicator:
    def __init__(self):
        self.vec_label = []
        self.prob_matrix = []

    def extend_labels(self, labels: np.array):
        self.vec_label.append(labels)

    def extend_proba(self, proba: np.array):
        self.prob_matrix.append(proba)

    def get_noisy_indices(self):
        self.vec_label = np.vstack(self.vec_label)
        self.prob_matrix = np.vstack(self.prob_matrix)
        s = np.zeros((self.vec_label.shape[0], 3))

        for i in range(self.vec_label.shape[0]):
            sj = 0
            for j in range(self.vec_label.shape[1]):
                if self.vec_label[i][j] == 1:
                    s[i][sj] = j
                    sj += 1
        # prune_method=? можно попробовать разные
        return get_noise_indices(s, self.prob_matrix, multi_label=True)
