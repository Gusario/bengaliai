from typing import List
import cv2
import random

import numpy as np
from albumentations.core.transforms_interface import ImageOnlyTransform, DualTransform


class OneToThree(ImageOnlyTransform):
    """Transforms image from shape (..., 1) to (..., 3), for example (224, 224, 1) -> (224, 224, 3)"""

    def __init__(self, fake=None, always_apply=False, p=1.0):
        super(OneToThree, self).__init__(always_apply, p)
        self.fake = fake

    def apply(self, image: np.array, **params) -> np.array:
        image = image.reshape(image.shape[0], image.shape[1], 1)
        image = image.repeat(3, axis=2)
        return image

    def get_transform_init_args_names(self):
        return ("fake",)


class LafossResize(ImageOnlyTransform):
    def __init__(self, fake=None, always_apply=False, p=1.0):
        super(LafossResize, self).__init__(always_apply, p)
        self.fake = fake
        self.width = 236
        self.height = 137
        self.size = 128
        self.pad = 16

    def bbox(self, img):
        rows = np.any(img, axis=1)
        cols = np.any(img, axis=0)
        rmin, rmax = np.where(rows)[0][[0, -1]]
        cmin, cmax = np.where(cols)[0][[0, -1]]
        return rmin, rmax, cmin, cmax

    def normalize(self, img):
        img = img.reshape(137, 236)
        img = img / img.max()
        img = (img * 254).astype(np.uint8)
        return img

    def apply(self, img0, **param):
        """crop a box around pixels large than the threshold"""
        img0 = self.normalize(img0)

        ymin, ymax, xmin, xmax = self.bbox(img0[5:-5, 5:-5] > 80)
        xmin = xmin - 13 if (xmin > 13) else 0
        ymin = ymin - 10 if (ymin > 10) else 0
        xmax = xmax + 13 if (xmax < self.width - 13) else self.width
        ymax = ymax + 10 if (ymax < self.height - 10) else self.height
        img = img0[ymin:ymax, xmin:xmax]
        img[img < 28] = 0
        lx, ly = xmax - xmin, ymax - ymin
        l = max(lx, ly) + self.pad
        img = np.pad(img, [((l - ly) // 2,), ((l - lx) // 2,)], mode="constant")
        rs = cv2.resize(img, (self.size, self.size))

        rs = rs.astype(np.float32) / 254

        return rs

        def get_transform_init_args_names(self):
            return ("fake",)


class MixImages:
    """Mix several imagers with different weights"""

    def __init__(self, images: List[np.array], weights: List[float]):
        self.images = self.images
        self.weights = weights

    def __call__(self, batch: dict) -> dict:
        main_image = batch["image"] * self.weights[0]
        for i, img in enumerate(self.images):
            main_image += img * self.weights[i + 1]

        batch["image"] = main_image
        return batch


class StackMatrices(ImageOnlyTransform):
    def __init__(self, matrices: List[np.array], always_apply=False, p=1.0):
        super().__init__(always_apply, p)
        shape = matrices[0].shape
        self.matrices = [mat.reshape(*shape, 1) for mat in matrices]

    def apply(self, image: np.array, **params):
        image = np.dstack((image, *self.matrices)).astype(np.float32)
        return image

    def get_transform_init_args_names(self):
        return ("matrices",)


class GridMask(DualTransform):
    """GridMask augmentation for image classification and object detection.

    Author: Qishen Ha
    Email: haqishen@gmail.com
    2020/01/29

    Args:
        num_grid (int): number of grid in a row or column.
        fill_value (int, float, lisf of int, list of float): value for dropped pixels.
        rotate ((int, int) or int): range from which a random angle is picked. If rotate is a single int
            an angle is picked from (-rotate, rotate). Default: (-90, 90)
        mode (int):
            0 - cropout a quarter of the square of each grid (left top)
            1 - reserve a quarter of the square of each grid (left top)
            2 - cropout 2 quarter of the square of each grid (left top & right bottom)

    Targets:
        image, mask

    Image types:
        uint8, float32

    Reference:
    |  https://arxiv.org/abs/2001.04086
    |  https://github.com/akuxcw/GridMask
    """

    def __init__(self, num_grid=3, fill_value=0, rotate=0, mode=0, always_apply=False, p=0.5):
        super(GridMask, self).__init__(always_apply, p)
        if isinstance(num_grid, int):
            num_grid = (num_grid, num_grid)
        if isinstance(rotate, int):
            rotate = (-rotate, rotate)
        self.num_grid = num_grid
        self.fill_value = fill_value
        self.rotate = rotate
        self.mode = mode
        self.masks = None
        self.rand_h_max = []
        self.rand_w_max = []

    def init_masks(self, height, width):
        if self.masks is None:
            self.masks = []
            n_masks = self.num_grid[1] - self.num_grid[0] + 1
            for n, n_g in enumerate(range(self.num_grid[0], self.num_grid[1] + 1, 1)):
                grid_h = height / n_g
                grid_w = width / n_g
                this_mask = np.ones((int((n_g + 1) * grid_h), int((n_g + 1) * grid_w))).astype(np.uint8)
                for i in range(n_g + 1):
                    for j in range(n_g + 1):
                        this_mask[
                            int(i * grid_h) : int(i * grid_h + grid_h / 2),
                            int(j * grid_w) : int(j * grid_w + grid_w / 2),
                        ] = self.fill_value
                        if self.mode == 2:
                            this_mask[
                                int(i * grid_h + grid_h / 2) : int(i * grid_h + grid_h),
                                int(j * grid_w + grid_w / 2) : int(j * grid_w + grid_w),
                            ] = self.fill_value

                if self.mode == 1:
                    this_mask = 1 - this_mask

                self.masks.append(this_mask)
                self.rand_h_max.append(grid_h)
                self.rand_w_max.append(grid_w)

    def apply(self, image, mask, rand_h, rand_w, angle, **params):
        h, w = image.shape[:2]
        mask = F.rotate(mask, angle) if self.rotate[1] > 0 else mask
        mask = mask[:, :, np.newaxis] if image.ndim == 3 else mask
        image *= mask[rand_h : rand_h + h, rand_w : rand_w + w].astype(image.dtype)
        return image

    def get_params_dependent_on_targets(self, params):
        img = params["image"]
        height, width = img.shape[:2]
        self.init_masks(height, width)

        mid = np.random.randint(len(self.masks))
        mask = self.masks[mid]
        rand_h = np.random.randint(self.rand_h_max[mid])
        rand_w = np.random.randint(self.rand_w_max[mid])
        angle = np.random.randint(self.rotate[0], self.rotate[1]) if self.rotate[1] > 0 else 0

        return {"mask": mask, "rand_h": rand_h, "rand_w": rand_w, "angle": angle}

    @property
    def targets_as_params(self):
        return ["image"]

    def get_transform_init_args_names(self):
        return ("num_grid", "fill_value", "rotate", "mode")


class RandomMorph(ImageOnlyTransform):
    def __init__(self, _min=2, _max=6, element_shape=cv2.MORPH_ELLIPSE, always_apply=False, p=0.5):
        super().__init__(always_apply, p)
        self._min = _min
        self._max = _max
        self.element_shape = element_shape

    def apply(self, image, **params):
        arr = np.random.randint(self._min, self._max, 2)
        kernel = cv2.getStructuringElement(self.element_shape, tuple(arr))

        if random.random() > 0.5:
            # make it thinner
            image = cv2.erode(image, kernel, iterations=1)
        else:
            # make it thicker
            image = cv2.dilate(image, kernel, iterations=1)

        return image

    def get_transform_init_args_names(self):
        return ("_min", "_max", "element_shape")


class LabelSmoothing:
    def __init__(self, eps, K, len_positive_labels=1):
        self.eps = eps
        self.K = K
        self.len_positive_labels = 3

    def __call__(self, sample):  ## TODO to vector
        for i in range(len(sample)):
            if sample[i] < 0.00001:
                sample[i] = self.eps / ((self.K - self.len_positive_labels) / self.len_positive_labels)
            else:
                sample[i] = 1 - self.eps

        return sample
