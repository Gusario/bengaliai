import os

from pytorch_lightning.callbacks import ModelCheckpoint, EarlyStopping
from pytorch_lightning.logging import TestTubeLogger


def add_file_hash_and_save(file_name, config):
    path = f"/src/workspace/src/{file_name}.py"

    with open(path, "r") as f:
        all_text = f.read()

    all_text = "".join(all_text.split())
    config_hash = hash(all_text)
    config["name"] += str(config_hash)
    os.system(f"cp /src/workspace/src/{file_name}.py /src/workspace/configs/{config['name']}.py")  # save config
    return config


def setup_lightning_callbacks_and_loggers(config: dict, name: str) -> None:
    filepath = os.path.join(config["lightning_callbacks"]["dirpath"], name)

    # config["lightning_callbacks"]["ModelCheckpoint_kwargs"]["filepath"] = filepath
    checkpoint = ModelCheckpoint(filepath, **config["lightning_callbacks"]["ModelCheckpoint_kwargs"])
    config["trainer"]["checkpoint_callback"] = checkpoint

    earlystop = EarlyStopping(**config["lightning_callbacks"]["EarlyStopping_kwargs"])
    config["trainer"]["early_stop_callback"] = earlystop

    logger = TestTubeLogger(
        os.path.join(config["lightning_callbacks"]["logger_base_dir"], name),
        **config["lightning_callbacks"]["TestTubeLogger_kwargs"],
    )
    logger.experiment.tag(config)
    config["trainer"]["logger"] = logger
    return config
