import numpy as np


class PositionalEncoding:
    """Generate positional encoding mask"""

    def __init__(self, height: int, width: int, kind="l2"):
        self._height, self._width = height, width
        if kind == "l2":
            self._dist_function = self._l2
        elif kind == "l1":
            self._dist_function = self._l1
        else:
            raise NotImplementedError(f"{kind} is not implemented, change kind")
        self._mask = self._generate_mask()
        self._fill_mask()
        self._normalize()

    def __call__(self) -> np.array:
        return self._mask

    def _generate_mask(self) -> np.array:
        return np.zeros((self._height, self._width))

    def _fill_mask(self) -> None:
        center = np.array([self._mask.shape[0] // 2, self._mask.shape[1] // 2])
        for i in range(self._mask.shape[0]):
            for j in range(self._mask.shape[1]):
                self._mask[i, j] = self._dist_function(i, j, center)

    def _normalize(self) -> None:
        max_ = self._mask.max()
        self._mask = self._mask / max_
        self._mask = 1 - self._mask

    def _l2(self, i: int, j: int, center: np.array) -> float:
        return np.sqrt((i - center[0]) ** 2 + (j - center[1]) ** 2)

    def _l1(self, i: int, j: int, center: np.array) -> float:
        return np.abs(i - center[0]) + np.abs(j - center[1])
