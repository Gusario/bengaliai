import argparse
from importlib import import_module
from shutil import copyfile, rmtree
import os

from bengaliai.send_answer import send_answer


def copy_to_temp(path, destination):
    os.makedirs(destination, exist_ok=True)
    name = "_".join(path.split("/")[-2:])
    copyfile(path, os.path.join(destination, name))


if __name__ == "__main__":
    TEMP_FOLDER = "/src/workspace/models/temp_folder"

    parser = argparse.ArgumentParser(description="run config")
    parser.add_argument("--config", type=str, help="path to module", default="predict_config")
    args = parser.parse_args()
    config_module = args.config
    config_module = import_module(config_module)  # load config as python module
    configs = config_module.configs
    rmtree(TEMP_FOLDER, ignore_errors=True)
    for config in configs:
        path_to_checkpoint = config["path_to_checkpoint"]
        copy_to_temp(path_to_checkpoint, TEMP_FOLDER)


send_answer(name_id="kekblyad48", path_to_weights=TEMP_FOLDER, code_file="predict.py", path_to_files="./")
